package com.example.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.api.models.SecurityOperationsCenterModel;

@Repository
public interface SecurityOperationsCenterRepository extends JpaRepository<SecurityOperationsCenterModel,Integer>{
    
    @Query(value = "SELECT * FROM soc_table WHERE token LIKE :token%",nativeQuery = true)
    SecurityOperationsCenterModel findByToken(String token);

}
