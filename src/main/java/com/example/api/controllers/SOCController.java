package com.example.api.controllers;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.api.entity.Body;
import com.example.api.entity.Token;
import com.example.api.models.SecurityOperationsCenterModel;
import com.example.api.repositories.SecurityOperationsCenterRepository;
import com.example.api.security.JwtUtil;
import com.example.api.services.UserService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class SOCController {
    
    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    SecurityOperationsCenterRepository securityOperationsCenterRepository;
    
    @PostMapping("/createUser")
    public ResponseEntity<SecurityOperationsCenterModel> createUser(@Valid @RequestBody SecurityOperationsCenterModel pSOC){
        try {
            SecurityOperationsCenterModel socUser = new SecurityOperationsCenterModel(); //Create new SOC User

            //Set infomation for new token
            Token token = new Token();
            token.setToken(jwtUtil.generateToken(pSOC.getSoc_name()));
            token.setTokenExpDate(jwtUtil.generateExpirationDate());

            //Set infomation to SOC user
            socUser.setSoc_name(pSOC.getSoc_name());
            socUser.setOrgs(pSOC.getOrgs());
            socUser.setRestrict_interval(pSOC.getRestrict_interval());
            socUser.setToken(token.getToken());

            //Save infomation of SOC user to database 
            SecurityOperationsCenterModel saveSOCUser = securityOperationsCenterRepository.save(socUser);
            return new ResponseEntity<>(saveSOCUser,HttpStatus.CREATED); //Response with status 201(Create success)
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/checkValidate")
    public ResponseEntity<String> checkValidate(@Valid @RequestBody Body pBody){
        try {
            return new ResponseEntity<>("Dữ liệu đúng",HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>("Dữ liệu chưa đúng",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/getAllSoc")
    public ResponseEntity<List<SecurityOperationsCenterModel>> getAllSoc(){
        try {
            List<SecurityOperationsCenterModel> socs = new ArrayList<>();
            securityOperationsCenterRepository.findAll().forEach(socs::add);
            return new ResponseEntity<>(socs,HttpStatus.OK);
        } catch (Exception e){  
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PreAuthorize("hasRole('TEST1')")
    @GetMapping("/checkAuthorize")
    public String checkRole(){
        return "Check role success";
    }

}
