package com.example.api.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "orgnization_table")
public class OrgnizationModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "`org-name`", unique = true)
    private String orgName;

    @ManyToOne
    @JoinColumn(name = "`soc-id`")
    @JsonIgnore
    private SecurityOperationsCenterModel securityOperationCenter;

    public int getId() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public SecurityOperationsCenterModel getSecurityOperationCenter() {
        return securityOperationCenter;
    }

    public void setSecurityOperationCenter(SecurityOperationsCenterModel securityOperationCenter) {
        this.securityOperationCenter = securityOperationCenter;
    }

}
