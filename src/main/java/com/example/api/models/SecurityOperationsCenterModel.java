package com.example.api.models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "soc_table") //Table for security operations center employee

public class SecurityOperationsCenterModel {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "`soc-id`")
    private int soc_id;

    @Column(name = "`soc-name`",unique = true)
    private String soc_name;

    @Column(name = "token",unique = true)
    private String token;

    @Column(name = "`restrict-interval`")
    private int restrict_interval;

    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinTable(name = "soc_org_table", joinColumns = { @JoinColumn(name = "`soc-id`") }, inverseJoinColumns = {@JoinColumn(name = "`org-id`")})
    private Set<OrgnizationModel> orgs = new HashSet<>();

    public SecurityOperationsCenterModel(){

    }

    public int getSoc_id() {
        return soc_id;
    }

    public void setSoc_id(int soc_id) {
        this.soc_id = soc_id;
    }

    public String getSoc_name() {
        return soc_name;
    }

    public void setSoc_name(String soc_name) {
        this.soc_name = soc_name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getRestrict_interval() {
        return restrict_interval;
    }

    public void setRestrict_interval(int restrict_interval) {
        this.restrict_interval = restrict_interval;
    }

    public Set<OrgnizationModel> getOrgs() {
        return orgs;
    }

    public void setOrgs(Set<OrgnizationModel> orgs) {
        this.orgs = orgs;
    }

    @Override
    public String toString() {
        return "SecurityOperationsCenterModel [soc_id=" + soc_id + ", soc_name=" + soc_name + ", token=" + token
                + ", restrict_interval=" + restrict_interval + ", orgs=" + orgs + "]";
    }
    
}
