package com.example.api.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "update_history_table")
public class UpdateHistoryModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @Column(name = "`org-id`")
    private int org_id;

    @Column(name = "`soc-id`")
    private int soc_id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "`last-update`")
    private Date last_update;

    public UpdateHistoryModel() {

    }

    public int getOrg_id() {
        return org_id;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public int getSoc_id() {
        return soc_id;
    }

    public void setSoc_id(int soc_id) {
        this.soc_id = soc_id;
    }

    public Date getLast_update() {
        return last_update;
    }

    public void setLast_update(Date last_update) {
        this.last_update = last_update;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
