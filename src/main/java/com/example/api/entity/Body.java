package com.example.api.entity;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Body { //Class to validation request

    @NotEmpty(message = "Tên không được trống")
    private String name;

    @Min(value = 10,message = "Số phải lớn hơn 10")
    private Integer number;
    public Body(){

    }

}
