package com.example.api.services;

import org.springframework.stereotype.Service;

import com.example.api.models.SecurityOperationsCenterModel;
import com.example.api.security.UserPrincipal;

@Service
public interface UserService {

    SecurityOperationsCenterModel createUser(SecurityOperationsCenterModel user);

    UserPrincipal findByToken(String token);
    
}
