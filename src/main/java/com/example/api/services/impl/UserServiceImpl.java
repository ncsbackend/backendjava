package com.example.api.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.api.models.SecurityOperationsCenterModel;
import com.example.api.repositories.SecurityOperationsCenterRepository;
import com.example.api.security.UserPrincipal;
import com.example.api.services.UserService;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private SecurityOperationsCenterRepository securityOperationsCenterRepository;

    @Override
    public SecurityOperationsCenterModel createUser(SecurityOperationsCenterModel pSOC) {
        return securityOperationsCenterRepository.saveAndFlush(pSOC);
    }

    @Override
    public UserPrincipal findByToken(String token) {
        SecurityOperationsCenterModel soc = securityOperationsCenterRepository.findByToken(token);
        UserPrincipal userPrincipal = new UserPrincipal();
        if (null != soc) {
            Set<String> authorities = new HashSet<>();
            if (null != soc.getOrgs())
                soc.getOrgs().forEach(r -> {
                    authorities.add(r.getOrgName());
                });
            userPrincipal.setUserId(soc.getSoc_id());
            userPrincipal.setUsername(soc.getSoc_name());
            userPrincipal.setAuthorities(authorities);
        }
        return userPrincipal;
    }

}